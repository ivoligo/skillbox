
public class Cat
{
    //Урок 06
    private CatsColor colorCats;

    //Урок 4
    public static final int CATS_EYES=2;
    public static final Double WEIGHT_MIN = 1000.0;
    public static final Double WEIGHT_MAX = 9000.0;

    public static int count=0;

    private double originWeight;
    private double weight;

//    private double minWeight;
//    private double maxWeight;
    private double sumEat;

    private String name;

    public Cat()
    {
        weight = 1500.0 + 3000.0 * Math.random();
        originWeight = weight;
//        minWeight = 1000.0;
//        maxWeight = 9000.0;
        count++;

    }

    public void meow()
    {
        weight = weight - 1;
        System.out.println("Meow");
    }

    public void feed(Double amount)
    {
        if (!isAlive()) {
            weight = weight + amount;
            sumEat = +amount;
        } else {
            System.out.println("Кошка мертва, она не может есть");
        }
    }

    public void drink(Double amount)
    {
        if (!isAlive()) {
            weight = weight + amount;
        } else {
            System.out.println("Кошка мертва, она не может пить");
        }
    }

    public Double getWeight()
    {
        return weight;
    }

    public String getStatus()
    {
        if(weight < WEIGHT_MIN) {
            count--;
            return "Dead";
        }
        else if(weight > WEIGHT_MAX) {
            count--;
            return "Exploded";
        }
        else if(weight > originWeight) {
            return "Sleeping";
        }
        else {
            return "Playing";
        }
    }

    //урок 02
    public Double getEatSum(){
        return sumEat;
    }

    public void pee(){
        if (!isAlive()) {
            weight = weight - 100.0;
            System.out.println("мне полегчало");
        } else {
            System.out.println("кошка мертва, она не может писать");
        }
    }

    //Урок 03
    public static int getCount(){
        return count;
    }

    public Boolean isAlive(){
        return (weight > WEIGHT_MAX) || (weight < WEIGHT_MIN);
    }

    //Урок 04

    public void setCatsColor(CatsColor color){
        this.colorCats = color;
    }

    //Урок 05
    public Cat(Double weight){
        this.weight = weight;
        originWeight = weight;
//        minWeight = 1000.0;
//        maxWeight = 9000.0;
        count++;
    }

    //Урок06
    public CatsColor getColorCats(){
        return colorCats;

    }
    public void setColorCats(CatsColor colorCats){
        this.colorCats = colorCats;
    }

    //Урок07
   public Cat( String name, Double weight){
        this.weight = weight;
        this.name = name;

   }
   public Cat copyCat(){
       return new Cat(name, weight);
   }

   public String getName(){
        return name;
   }
   public void setName(String name){
        this.name = name;
   }
}