import javax.crypto.spec.PSource;

public class Loader
{
    public static void main(String[] args)
    {
        Cat cat = new Cat();

        System.out.println(cat.getStatus());

        //Урок 01
        Cat murzik = new Cat();
        Cat barsik = new Cat();
        Cat masya = new Cat();
        Cat vaska = new Cat();
        Cat gavrik = new Cat();
        Cat alkashik = new Cat();

        System.out.println(" murzik weight: " + murzik.getWeight() + "; \n" +
                        " barsik weight: " + barsik.getWeight() + "; \n" +
                        " masya weight: " + masya.getWeight() + "; \n" +
                        " vaska weight: " + vaska.getWeight() + "; \n" +
                        " gavrik weight: " + gavrik.getWeight() + "; \n" +
                        " alkashik weight: " + alkashik.getWeight() + ";\n");
        vaska.feed(3000.0);
        murzik.feed(1200.0);
        System.out.println("vaska поел и стал весить: " + vaska.getWeight() +";\n" + "murzik поел и стал весить: " + murzik.getWeight() + "\n");
        //я не поняол, что имеется ввиду под проверкой статуса и сделал так.
        for ( int i =0 ;  ; i++) {
            alkashik.drink(1000.0);
            if (alkashik.getStatus().equals("Exploded")){
                System.out.println("alkashik перепил: " + alkashik.getWeight() + "\n");
                break;
            }
        }
        while (!masya.getStatus().equals("Dead")){
            masya.meow();

        }
        System.out.println("бедная masya умерла, перемяукала: " + masya.getWeight() +" \n");

        //Урок 02
        System.out.println("Урок 02");
        Cat koshka = new Cat();
        koshka.feed(150.0);
        koshka.pee();
        koshka.pee();
        System.out.println("koshka съела  "+koshka.getEatSum());

        //Урок 03
        System.out.println("Урок 03");
        System.out.println("осталось кошек " + Cat.getCount());
        masya.pee();
        masya.drink(300.0);

        //урок 04
        vaska.setCatsColor(CatsColor.BLUE);


        //Урок 05
        System.out.println("Урок05");
        Cat malish = new Cat (1100.0);
        System.out.println("вес малыша " +malish.getWeight());

        getKitten();
        System.out.println("Котенок  весом " + getKitten().getWeight()+"\n");

        //Урок 06
        System.out.println("Урок 06");
        gavrik.setColorCats(CatsColor.AMBER);
        System.out.println(gavrik.getColorCats()+"\n");

        //урок 07
        System.out.println("Урок 06");
        Cat test = new Cat();
        test.setName("Петя");
        test.getWeight();
        Cat copyTest = test.copyCat();
//        Cat copyTest = new Cat(test.getName(), test.getWeight());
        System.out.println(" кошка test имя: " + test.getName() + ", вес " + test.getWeight() + ", адрес "+test);
        System.out.println(" кошка copyTest имя: " + copyTest.getName() + ", вес " + copyTest.getWeight() + ", адрес " + copyTest);
    }

    //Урок05
    private static Cat getKitten(){
        return new Cat(1100.0);
    }
}