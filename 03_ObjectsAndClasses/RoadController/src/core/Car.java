package core;

public class Car
{
    private String number;
    private int height;
    private double weight;
    private boolean hasVehicle;
    private boolean isSpecial;

    public String toString()
    {
        String special = isSpecial ? "СПЕЦТРАНСПОРТ " : "";
        return "\n=========================================\n" +
            special + "Автомобиль с номером " + number +
            ":\n\tВысота: " + height + " мм\n\tМасса: " + weight + " кг";
    }

    public String getNumber(String number){
        return number;
    }
    public void setNumber(String number){
        this.number = number;
    }

    public int getHeight(int height){
        return height;
    }
    public void setHeight(int height){
        this.height = height;
    }

    public Double getWeight(double weight){
        return weight;
    }
    public void setWeight(double weight){
        this.weight = weight;
    }

    public boolean isHasVehicle(){
        return hasVehicle;
    }
    public void setHasVehicle(Boolean hasVehicle){
        this.hasVehicle = hasVehicle;
    }

    public boolean isSpecial(){
        return isSpecial;
    }

    public void setSpecial (boolean special){
        this.isSpecial = special;
    }

}