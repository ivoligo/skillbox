import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.RecursiveTask;

public class PageGetLinks extends RecursiveTask<Set<String>> {

    private Set<String> urls;

    public PageGetLinks(Set<String> urls){
        this.urls = urls;
    }

    @Override
    protected Set<String> compute() {

        List<PageGetLinks> taskList = new ArrayList<>();
        Set<String> test = new TreeSet<>();

        for (String child : urls){
            PageGetLinks task = new PageGetLinks(ParsePage.getChildren(child));
            task.fork();
            taskList.add(task);
        }

        return test;
    }
}
