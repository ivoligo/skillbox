import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class WriteSiteMapInFile {

    public static void write(Set<String> links, String destination) throws IOException {

        Set<String> set = new LinkedHashSet<>();
        links.forEach(link -> {
            StringBuilder buffer = new StringBuilder();
            String[] split = link.substring(Main.PAGE.length()-1).split("/");
            set.add(buffer.append("\t".repeat(split.length) + link).toString());
        });
        String[] split = destination.split("/");
        StringBuilder stringBuilder = new StringBuilder();
        for(int i = 0; i < split.length-1 ; i++){
            stringBuilder.append(split[i]);
            if(split[i].equals(".") && !split[i+1].equals(".")){
                stringBuilder.append("/");
            }
        }
        File destinationFolder = new File(stringBuilder.toString());
        destinationFolder.mkdir();
        Files.write(Paths.get(destination), set);
    }
}
