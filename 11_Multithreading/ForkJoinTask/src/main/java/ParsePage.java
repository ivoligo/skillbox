import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.*;

public class ParsePage{

    static Set<String> pageMap = new TreeSet<>();

    public static Set<String> getChildren(String url) {

        Set<String> links = new TreeSet<>();
        Document doc;

        try {
            doc = Jsoup.connect(url).maxBodySize(0).get();
            Elements elements = doc.getElementsByAttribute("href");
            Thread.sleep(150);

            elements.forEach(element -> {
                String link = element.attr("abs:href");
                if(link.startsWith(url) && link.endsWith("/")){
                    synchronized (pageMap) {
                        if(!pageMap.contains(link)) {
                            pageMap.add(link);
                            links.add(link);
                        }
                    }
                }
                if(link.startsWith("/") ){
                    link = Main.PAGE + link.substring(1);
                    synchronized (pageMap) {
                        if(!pageMap.contains(link)) {
                            pageMap.add(link);
                            links.add(link);
                        }
                    }
                }
                /*
                    считается отдельной страницей https://skillbox.ru/design/?page=2
                    или же это относится к странице https://skillbox.ru/design/
                    ?
                    если считается, то необходимо расскоментить код ниже
                */
//                if(link.startsWith(url) && link.contains("/?")){
//                    synchronized (pageMap) {
//                        if(!pageMap.contains(link)) {
//                            pageMap.add(link);
//                            links.add(link);
//                        }
//                    }
//                }
            });

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }

        return links;
    }

}
