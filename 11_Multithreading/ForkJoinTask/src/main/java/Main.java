import java.io.IOException;
import java.util.concurrent.ForkJoinPool;

public class Main {

    static final String PAGE = "https://skillbox.ru/";
    static final String FILE = "./data/siteMap.txt";

    public static void main(String[] args) throws IOException {

        new ForkJoinPool().invoke(new PageGetLinks(ParsePage.getChildren(PAGE)));
        WriteSiteMapInFile.write(ParsePage.pageMap, FILE);

    }
}
