import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;

public class Main {

    private static final int newWidth = 300;

    public static void main(String[] args) throws InterruptedException {
        String srcFolder = "c:/skillbox/srcImg";
        String dstFolder = "c:/skillbox/dstImg";

        File srcDir = new File(srcFolder);
        File[] files = srcDir.listFiles();
        long start = System.currentTimeMillis();
        int perCore = files.length / 4;
        File[] files1 = new File[perCore];
        System.arraycopy(files, 0, files1, 0, files1.length);
        Resizer resizer1 = new Resizer(files1, dstFolder, newWidth);

        File[] files2 = new File[perCore+1];
        System.arraycopy(files, perCore, files2, 0, files2.length);
        Resizer resizer2 = new Resizer(files2, dstFolder, newWidth);

        File[] files3 = new File[perCore+1];
        System.arraycopy(files, perCore, files3, 0, files3.length);
        Resizer resizer3 = new Resizer(files3, dstFolder, newWidth);

        File[] files4 = new File[perCore+1];
        System.arraycopy(files, perCore, files4, 0, files4.length);
        Resizer resizer4 = new Resizer(files4, dstFolder, newWidth);
        resizer1.join();
        resizer2.join();
        resizer3.join();
        resizer4.join();

        System.out.println("Duration: " + (System.currentTimeMillis() - start));
    }
}
