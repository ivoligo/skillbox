import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Bank{
    private Set<Account> accounts;
    private final Random random = new Random();
    private Account accountFrom;
    private Account accountTo;

    public Bank(Set<Account> accounts) {
        this.accounts = accounts;
    }

    public synchronized boolean isFraud(String fromAccountNum, String toAccountNum, long amount)
        throws InterruptedException {
        Thread.sleep(1000);
        return random.nextBoolean();
    }

    /**
     * TODO: реализовать метод. Метод переводит деньги между счетами.
     * Если сумма транзакции > 50000, то после совершения транзакции,
     * она отправляется на проверку Службе Безопасности – вызывается
     * метод isFraud. Если возвращается true, то делается блокировка
     * счетов (как – на ваше усмотрение)
     */
    public void transfer(String fromAccountNum, String toAccountNum, long amount) {

        for (Account account : accounts) {
            if (account.getAccNumber().equals(fromAccountNum)) {
                accountFrom = account;
            }
            if (account.getAccNumber().equals(toAccountNum)) {
                accountTo = account;
            }
        }

        if (accountFrom.getMoney() - amount < 0) {
            System.out.println("Перевод со счета "
                    + accountFrom.getAccNumber() + " на счет "
                    + accountTo.getAccNumber() + " невозможен. Недостаточно средств на счете");
        } else {
            if (amount > 50000) {
                synchronized (accountFrom) {
                    synchronized (accountTo) {
                        System.out.println("перевод между счетами " + fromAccountNum + " и " + toAccountNum + " на проверке в службе безопасности ");
                        try {
                            if (isFraud(fromAccountNum, toAccountNum, amount)) {

                                System.out.println("Внимание! Службой безопасности заблокированы счета: "
                                        + accountFrom.getAccNumber() + " и "
                                        + accountTo.getAccNumber());
                            } else {
                                makeTransfer(amount);
                            }
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
            } else {
                makeTransfer(amount);
            }

        }
    }

    private void makeTransfer(long amount) {
        accountFrom.setMoney(accountFrom.getMoney() - amount);
        accountTo.setMoney(accountTo.getMoney() + amount);
        System.out.println("Осуществлен перевод между счетами " + accountFrom.getAccNumber() + "  " + accountTo.getAccNumber() +
                " " + accountFrom.getMoney() + " " + accountTo.getMoney());
    }


    /**
     * TODO: реализовать метод. Возвращает остаток на счёте.
     */
    public synchronized long getBalance(String accountNum){
        long balance = 0;
        String acc = null;
        for(Account account : accounts) {
            if (account.getAccNumber().equals(accountNum)) {
                balance = account.getMoney();
//                acc = account.getAccNumber();
            }
        }
//        System.out.println("БАЛАНС счета № "+acc+ " = " +balance );
        return balance;
    }

    public Set<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }

}
