import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class Main {

    public static void main(String[] args) throws InterruptedException {
        Set<Account> accounts= new LinkedHashSet<>();

        Account account1 = new Account(123000, "1");
        Account account2 = new Account(103000, "2");
        Account account3 = new Account(100000, "3");
        Account account4 = new Account(130000, "4");
        Account account5 = new Account(730000, "5");

        accounts.add( account1);
        accounts.add( account2);
        accounts.add( account3);
        accounts.add( account4);
        accounts.add( account5);

        Bank bank = new Bank(accounts);
        long sumBefore = accounts.stream().mapToLong(Account::getMoney).sum();

        Thread thread1 = new Thread(() -> {
            bank.transfer("5", "1", 51000);
        });
        Thread thread2 = new Thread(() -> {
            bank.transfer("5", "2", 51000);
        });
        Thread thread3 = new Thread(() -> {
            bank.transfer("3", "4", 51000);
        });
        Thread thread4 = new Thread(() -> {
            bank.transfer("3", "4", 3439);
        });
        Thread thread5 = new Thread(() -> {
            bank.transfer("4", "2", 34390);
        });
        Thread thread6 = new Thread(() -> {
            bank.transfer("1", "4", 343009);
        });

        Thread thread12 = new Thread(() ->
            System.out.println("баланс счета №2 = " + bank.getBalance("2")));
        Thread thread15 = new Thread(() ->
                System.out.println("баланс счета №5 = " + bank.getBalance("5")));
        Thread thread13 = new Thread(() ->
                System.out.println("баланс счета №3 = " + bank.getBalance("3")));
        Thread thread14 = new Thread(() ->
                System.out.println("баланс счета №4 = " + bank.getBalance("4")));
        Thread thread11 = new Thread(() ->
                System.out.println("баланс счета №1 = " + bank.getBalance("1")));

        thread1.start();
        thread2.start();
//        thread3.start();
//        thread4.start();
//        thread3.join(); //проверка на то, что будет добавлять или нет после блокировки счета
        thread4.start();
        thread5.start();
        thread6.start();

        Thread.sleep(3000);

        thread11.start();
        thread12.start();
        thread13.start();
        thread14.start();
        thread15.start();

        Thread.sleep(3000);
        long sumAfter = accounts.stream().mapToLong(Account::getMoney).sum();
        System.out.println(sumBefore == sumAfter);

    }
}
