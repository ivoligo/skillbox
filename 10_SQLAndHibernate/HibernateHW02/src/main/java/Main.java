import model.Course;
import model.PurchaseList;
import model.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.PersistenceUnit;

public class Main {
    public static void main(String[] args) {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml")
                .build();
        Metadata metadata = new MetadataSources(registry)
                .getMetadataBuilder()
                .build();
        SessionFactory sessionFactory = metadata.getSessionFactoryBuilder().build();
        Session session = sessionFactory.openSession();

        Course course = session.get(Course.class, 3);
        System.out.println("На курсе \"" +course.getName() + "\" обучается " + course.getStudentCount() + " студентов");

        Student student = session.get(Student.class, 1);
        System.out.println("Студент " + student.getName() + " зарегистрировался " + student.getRegistrationDate());

//        PurchaseList purchaseList = session.get(PurchaseList.class, 1);
//        System.out.println(purchaseList.getStudentName() + " " + purchaseList.getCourseName() + " " + purchaseList.getSubscriptionDate());

        sessionFactory.close();
    }
}
