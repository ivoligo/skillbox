package model;

import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;
import java.util.Objects;


@Embeddable
public class KeyPerStudentNameCourseName implements Serializable {

    public KeyPerStudentNameCourseName(){
    }

    @NotNull
    @Column(name = "student_name")
    private String studentName;

    @NotNull
    @Column(name = "course_name")
    private String courseName;

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public String getCourseName() {
        return courseName;
    }

    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        KeyPerStudentNameCourseName that = (KeyPerStudentNameCourseName) o;

        if (!Objects.equals(studentName, that.studentName)) return false;
        return Objects.equals(courseName, that.courseName);
    }

    @Override
    public int hashCode() {
        int result = studentName != null ? studentName.hashCode() : 0;
        result = 31 * result + (courseName != null ? courseName.hashCode() : 0);
        return result;
    }
}
