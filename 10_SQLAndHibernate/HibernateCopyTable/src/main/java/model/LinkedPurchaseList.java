package model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "LinkedPurchaseList")
public class LinkedPurchaseList {

    @EmbeddedId
    private KeyPerStudentIdCourseId id;

    public KeyPerStudentIdCourseId getId() {
        return id;
    }

    public void setId(KeyPerStudentIdCourseId id) {
        this.id = id;
    }

}
