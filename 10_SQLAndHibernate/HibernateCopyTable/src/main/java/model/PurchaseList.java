package model;

import com.sun.istack.NotNull;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "Purchaselist")
public class PurchaseList implements Serializable {

    @EmbeddedId
    private KeyPerStudentNameCourseName id;

    private int price;

    @Column(name = "subscription_date")
    private Date subscriptionDate;

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public Date getSubscriptionDate() {
        return subscriptionDate;
    }

    public void setSubscriptionDate(Date subscriptionDate) {
        this.subscriptionDate = subscriptionDate;
    }

    public KeyPerStudentNameCourseName getId() {
        return id;
    }

    public void setId(KeyPerStudentNameCourseName id) {
        this.id = id;
    }

//    @ManyToOne(cascade = CascadeType.ALL)
//    @JoinColumn(
//            name = "course_name",  insertable = false, updatable = false,
//            referencedColumnName = "name"
//    )
//    private Course course;
//
//    public Course getCourse() {
//        return course;
//    }
//
//    public void setCourse(Course course) {
//        this.course = course;
//    }

}
