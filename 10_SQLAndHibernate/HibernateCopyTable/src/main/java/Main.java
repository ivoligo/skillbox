import model.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import java.util.List;

public class Main {

    public static void main(String[] args) {

        int courseIdForInsert;
        int studentIdForInsert;
        String nameCourse;
        String nameStudent;
        LinkedPurchaseList elementForInsertToLinkedPurchaseList;

        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml")
                .build();
        Metadata metadata = new MetadataSources(registry)
                .getMetadataBuilder()
                .build();
        SessionFactory sessionFactory = metadata
                .getSessionFactoryBuilder()
                .build();
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        String hql = "from  PurchaseList";
        List<PurchaseList> purchaseLists = session.createQuery(hql).getResultList();

        for(PurchaseList purchaseList : purchaseLists){

            nameCourse = purchaseList.getId().getCourseName();
            nameStudent = purchaseList.getId().getStudentName();

            courseIdForInsert = (Integer) session.createQuery("select id from Course where name  =:nameCourse ")
                    .setParameter("nameCourse", nameCourse)
                    .getSingleResult();

            studentIdForInsert = (Integer) session.createQuery("select id from Student where name  =:nameStudent ")
                    .setParameter("nameStudent", nameStudent)
                    .getSingleResult();

            elementForInsertToLinkedPurchaseList = new LinkedPurchaseList();
            elementForInsertToLinkedPurchaseList.setId(new KeyPerStudentIdCourseId(studentIdForInsert, courseIdForInsert));
            session.save(elementForInsertToLinkedPurchaseList);
        }

        transaction.commit();
        sessionFactory.close();
    }
}
