import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Main {
    public static void main(String[] args) {
        String url ="jdbc:mysql://localhost:3306/skillbox?serverTimezone=UTC";
        String user = "root";
        String pass = "jav@MySQ1";

        try {
            Connection conn = DriverManager.getConnection(url, user, pass);
            Statement statement = conn.createStatement();
            ResultSet resultSet= statement.executeQuery("SELECT pl.course_name, COUNT(pl.course_name) / (MAX(MONTH(pl.subscription_date)) - MIN(MONTH(pl.subscription_date)) + 1) FROM PurchaseList pl group by pl.course_name;");

            while (resultSet.next()){
                System.out.print(resultSet.getString("course_name") + " | ");
                System.out.println(resultSet.getString(2));
//                System.out.println(resultSet.getString(3));
            }

            resultSet.close();
            statement.close();

            conn.close();
        } catch (Exception ex){
            ex.printStackTrace();
        }

    }


}
