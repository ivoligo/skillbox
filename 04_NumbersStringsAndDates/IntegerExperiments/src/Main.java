import static java.lang.Integer.valueOf;

public class Main
{
    public static void main(String[] args)
    {
        Container container = new Container();
        container.count += 7843;
        System.out.println(sumDigits(321));

        //задание со *
        char ch0 = 56;
        char ch1 = 'h';
        System.out.println("Значение int для ch0: "+ Character.getNumericValue(ch0) + "\n"+ "Значение int для ch1: " + Character.getNumericValue(ch1));



    }

    public static Integer sumDigits(Integer number)
    {
        //@TODO: write code here
        int sum = 0;
        String test = number.toString();
        for (int i = 0; i < test.length(); i++){
            sum += Integer.parseInt(String.valueOf(test.charAt(i)));
        }
        return sum;
    }
}
