import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите Фамилию, Имя, Отчество или exit для выхода");
        String fio;
        String out;
        String test = "-";
        while (sc.hasNext()){
            fio = sc.nextLine();
            if (fio.equalsIgnoreCase("exit")){
                break;
            }
            String[] fio_arr = fio.split("\\s+");
            System.out.println(fio.replaceAll("[^0-9]", ""));
            // добавил проверку, если фамилияя, имя, отчество пишется через дефис. вроде работает, но мог все не учесть
            if (fio_arr.length != 3 || !(fio.replaceAll("[^0-9]", "").isEmpty())){
                System.out.println("Вы ввели некорректные данные, повторите ввод");

            } else {
                for (int i = 0; i < fio_arr.length; i++) {
                    if (i == 0) {
                        out = fio_arr[i];
                        System.out.println("Фамилия: " + out);
                    }
                    if (i == 1) {
                        out = fio_arr[i];
                        System.out.println("Имя: " + out);
                    }
                    if (i == 2) {
                        out = fio_arr[i];
                        System.out.println("Отчество: " + out);
                    }
                }
                System.out.println("Вы можете продолжит, введя ФИО или выйти набрав exit");
            }


        }
    }
}
