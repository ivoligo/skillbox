public class Main {
    public static void main(String[] args) {
        System.out.println("Максимальное число у типа byte: " + Byte.MAX_VALUE);
        System.out.println("Минимальное число у типа byte: " + Byte.MIN_VALUE + "\n");
        System.out.println("Максимальное число у типа int: " + Integer.MAX_VALUE);
        System.out.println("Минимальное число у типа int: " + Integer.MIN_VALUE + "\n");
        System.out.println("Максимальное число у типа short: " + Short.MAX_VALUE);
        System.out.println("Минимальное число у типа short: " + Short.MIN_VALUE + "\n");
        System.out.println("Максимальное число у типа long: " + Long.MAX_VALUE);
        System.out.println("Минимальное число у типа long: " + Long.MIN_VALUE + "\n");
        System.out.println("Максимальное число у типа float: " + Float.MAX_VALUE);
        System.out.println("Минимальное число у типа float: " + (-Float.MAX_VALUE) + "\n");
        System.out.println("Максимальное число у типа double: " + Double.MAX_VALUE);
        System.out.println("Минимальное число у типа double: " + (-Double.MAX_VALUE) + "\n");
    }
}
