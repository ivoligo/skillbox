import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Main {
    public static void main(String[] args) {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy - E");
        int bithdayYear = 1983;
        Calendar calendarCount = Calendar.getInstance();
        calendarCount.set(bithdayYear, 2, 26);
        Calendar calendarNow = Calendar.getInstance();

        long count = (calendarNow.getTimeInMillis() - calendarCount.getTimeInMillis());
        count = (long) (ChronoUnit.DAYS.between(calendarCount.getTime().toInstant(), calendarNow.getTime().toInstant())/365.2425);
        System.out.println(count);

        for (int i = 0 ; i <= count ; i++ ) {
            calendarCount.set(bithdayYear + i, Calendar.FEBRUARY, 26);
            System.out.println(i + " - " + dateFormat.format(calendarCount.getTime()));
        }
    }
}
