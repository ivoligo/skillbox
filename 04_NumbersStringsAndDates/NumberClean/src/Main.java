import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("введите номер телефона или exit для выхода:");
        String phoneNumberIn ="";
        String phoneNumberOut = "+7";
        StringBuilder builder = new StringBuilder();
        builder.append(phoneNumberOut);

        while (sc.hasNextLine()){
            phoneNumberIn = sc.nextLine();
            if (phoneNumberIn.equalsIgnoreCase("exit")) {
                break;
            }
            phoneNumberIn = phoneNumberIn.replaceAll("[^0-9]", "");
            if (phoneNumberIn.length() > 10){
                phoneNumberIn = phoneNumberIn.replaceFirst("[0-9]", "");
            }
            if (phoneNumberIn.length() < 10){
                System.out.println("Вы ввели некорректные данные");
            } else {
                builder.append(" ").
                        append(phoneNumberIn.substring(0, 3)).
                        append(" ").
                        append(phoneNumberIn.substring(3, 6)).
                        append("-").
                        append(phoneNumberIn.substring(6, 8)).
                        append("-").
                        append(phoneNumberIn.substring(8));
                System.out.println(builder.toString());

                builder = new StringBuilder();
                builder.append(phoneNumberOut);
                System.out.println("Вы можете ввести номер телефона или если хотите выйти введите exit");
            }
        }
    }
}
