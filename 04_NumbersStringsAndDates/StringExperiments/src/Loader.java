
public class Loader
{
    public static void main(String[] args)
    {
        /*  В проекте StringExperiments внести изменения таким образом,
            чтобы суммы заработков каждого человека извлекались из текста
            регулярным выражением, а в конце программы рассчитывалась и
            распечатывалась общая сумма заработка всех.
         */
        System.out.println("сумма заработка Васи и Маши:");
        String text = "Вася заработал 5000 рублей, Петя - 7563 рубля, а Маша - 30000 рублей";
        text = text.replaceAll("[^0-9,]", "");
        int sum = 0;
        String[] salaryForOne = text.split(",");
        for (String value : salaryForOne) {
            sum += Integer.parseInt(value);
        }
        System.out.println(sum);

        /*
        Возьмите английский текст (не менее 100 слов) и напишите программу,
        которая будет разбивать его на слова и печатать слова в консоли.
         */
        System.out.println("разбивка слов");
        String englishText = "Java is a general-purpose programming language that is class-based, object-oriented,\n"+
                " and designed to have as few implementation dependencies as possible. It is intended to let application \n" +
                "developers write once, run anywhere (WORA), meaning that compiled Java code can run on all platforms \n"+
                "that support Java without the need for recompilation. Java applications are typically compiled to bytecode \n"+
                "that can run on any Java virtual machine (JVM) regardless of the underlying computer architecture. \n"+
                "The syntax of Java is similar to C and C++, but it has fewer low-level facilities than either of them. \n"+
                "As of 2019, Java was one of the most popular programming languages in use according to GitHub, particularly \n"+
                "for client-server web applications, with a reported 9 million developers.\n" +
                "Java was originally developed by James Gosling at Sun Microsystems (which has since been acquired by Oracle) \n"+
                "and released in 1995 as a core component of Sun Microsystems Java platform. The original and reference \n"+
                "implementation Java compilers, virtual machines, and class libraries were originally released by Sun under \n"+
                "proprietary licenses. As of May 2007, in compliance with the specifications of the Java Community Process, \n"+
                "Sun had relicensed most of its Java technologies under the GNU General Public License. Meanwhile, others have \n"+
                "developed alternative implementations of these Sun technologies, such as the GNU Compiler for Java (bytecode compiler), \n"+
                "GNU Classpath (standard libraries), and IcedTea-Web (browser plugin for applets).";
        englishText = englishText.replaceAll("[0-9,.)(]" , " ");
        englishText = englishText.replaceAll(" \\s+", " ");
        // не понял как объединить 41 и 42 строчку и возможно ли это вообще?. 42 строка убирает лишние пробелы
        String[] word = englishText.split(" ");
        for (String s : word) {
            System.out.println(s);
        }
    }
}