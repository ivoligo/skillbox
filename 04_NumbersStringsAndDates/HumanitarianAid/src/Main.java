import java.util.Scanner;
import java.util.concurrent.CountDownLatch;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите количество ящиков, оно должно быть целым: ");
        int numberOfBoxes = sc.nextInt();
        int countCar = 0;
        int countContainer = 0;
        int countBoxes;
        int car = 0;
        int boxesInContainer = 27;
        int containerInCar = 12;
        if (numberOfBoxes == 0){
            System.out.println("груз отсутствует");
        } else {
            for (int i = 0; i < numberOfBoxes; i++) {
                countBoxes = i % boxesInContainer;
                countCar = i % (containerInCar * boxesInContainer);
                if (countCar == 0) {
                    car++;
                    System.out.println("Грузовик " + car);
                }
                if (countBoxes == 0) {
                    countContainer++;
                    System.out.println("   Контейнер " + countContainer);
                }
                    System.out.println("     Ящик "+(i+1));
            }
        }
    }
}
