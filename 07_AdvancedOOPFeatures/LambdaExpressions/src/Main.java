import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Stream;

public class Main{
    private static String staffFile = "data/staff.txt";
    private static String dateFormat = "dd.MM.yyyy";

    public static void main(String[] args) throws ParseException {
        ArrayList<Employee> staff = loadStaffFromFile();

        Date date = new SimpleDateFormat("dd.MM.yyyy").parse("01.01.2017");

        staff.stream()
                .filter(employee -> employee.getWorkStart().after(date))
                .max(Comparator.comparing(Employee::getSalary)).ifPresent(System.out::println);

//        .filter(employee -> employee.getSalary() >= 130000).forEach(System.out:: println);
        /*
        Вопрос: если я сделаю выборку без даты(только выбирая всех сотрудников с максимальной зп),
        то выйдет первая попавшаяся зарплата удовлетворяющая условию (Анна Сетяева - 140000 - 10.05.2012).
        Как быть если сотрудников несколько? Ведь в данном примере несколько сотрудников с наибольшей зп.
        Что то не понял как вывести. это нужно с map делать?
         */

    }

    private static ArrayList<Employee> loadStaffFromFile(){
        ArrayList<Employee> staff = new ArrayList<>();
        try {
            List<String> lines = Files.readAllLines(Paths.get(staffFile));
            for(String line : lines){
                String[] fragments = line.split("\t");
                if(fragments.length != 3) {
                    System.out.println("Wrong line: " + line);
                    continue;
                }
                staff.add(new Employee(
                    fragments[0],
                    Integer.parseInt(fragments[1]),
                    (new SimpleDateFormat(dateFormat)).parse(fragments[2])
                ));
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        return staff;
    }
}