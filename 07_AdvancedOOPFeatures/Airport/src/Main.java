import com.skillbox.airport.Aircraft;
import com.skillbox.airport.Airport;
import com.skillbox.airport.Flight;
import com.skillbox.airport.Terminal;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.HOUR,2);
        SimpleDateFormat HOUR_FORMAT = new SimpleDateFormat("HH:mm");
        Airport airport = Airport.getInstance();
        List<Terminal> terminals = airport.getTerminals();
        terminals
                .forEach(t -> t.getFlights()
                        .stream()
                        .filter(f -> f.getType() == Flight.Type.ARRIVAL)
                        .filter(f -> f.getDate().after(Calendar.getInstance().getTime()))
                        .filter(f -> f.getDate().before(calendar.getTime()))
                        .forEach(r ->
                            System.out.println("Время вылета: " + HOUR_FORMAT.format(r.getDate()) +
                                    " модель самолета: " + r.getAircraft().getModel()))
                );
    }
}
