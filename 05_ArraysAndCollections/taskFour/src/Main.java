import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

public class Main {
    static TreeMap<String, String> phoneBook = new TreeMap<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String text;
        String name;
        String phoneNumberStr;
        int phoneNumber;

        System.out.println("введите имя или телефон для добавления в справочник, также list для вывода на экран или exit для выхода:");

        while (sc.hasNextLine()) {
            text = sc.nextLine();
            if (text.equalsIgnoreCase("exit")) {
                break;
            }

            if (text.equalsIgnoreCase("list")) {
                System.out.println("Телефонная книга:");
                printPhoneBook();
                System.out.println("введите имя или телефон для добавления в справочник, также list для вывода на экран или exit для выхода:");
            }
            else if (!text.replaceAll("[0-9]", "").isEmpty() && !phoneBook.containsKey(text) ) {
                System.out.println("Введите номер:");
                phoneNumberStr = sc.nextLine();
                phoneBook.put(text, phoneNumberStr);
                System.out.println("Добавлен в справочник: " + text + " " + phoneNumberStr);
                System.out.println("введите имя или телефон для добавления в справочник, также list для вывода на экран или exit для выхода:");
            } else if (!text.replaceAll("[^0-9]", "").isEmpty() && !phoneBook.containsValue(text) ) {
                System.out.println("Введите имя:");
                phoneNumberStr = text;
                text = sc.nextLine();
                phoneBook.put(text, phoneNumberStr);
                System.out.println("Добавлен в справочник: " + text + " " + phoneNumberStr);
                System.out.println("введите имя или телефон для добавления в справочник, также list для вывода на экран или exit для выхода:");
            } else if (phoneBook.containsKey(text)) {
                System.out.println("Запись с таким именем уже есть: ");
                System.out.println("Имя:" + text + " \n" + "номер: " + phoneBook.get(text) + "\n");
            } else if (phoneBook.containsValue(text)) {
                System.out.println("Запись с таким номером уже есть: ");
                System.out.println("Имя:" + findKeyPoValue(text) + " \n" + "номер: " + text + "\n");
                System.out.println("введите имя или телефон для добавления в справочник, также list для вывода на экран или exit для выхода:");
            }

        }
    }

    private static void printPhoneBook() {
        for (String key : phoneBook.keySet()) {
            System.out.println("Имя:" + key + " \n" + "номер: " + phoneBook.get(key) + "\n");
        }
    }

    private static String findKeyPoValue(String value) {
        String key = "";
        for (Map.Entry<String, String> pair : phoneBook.entrySet()) {
            if (value.equalsIgnoreCase(pair.getValue())) {
                key = pair.getKey();
            }
        }
        return key;
    }
}
