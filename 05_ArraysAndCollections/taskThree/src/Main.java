import java.util.HashSet;
import java.util.Scanner;

public class Main {
    static HashSet<String> emailSet = new HashSet<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String command;
        String email;
        String text;
        text = sc.nextLine();

        System.out.println("Введите команду:");
        while (sc.hasNextLine()){

            command = text.split("\\s+")[0];
            int test = text.split("\\s+").length;
            if (text.split("\\s+").length > 1) {
                email = text.split("\\s+")[1];
            } else {
                email = "";
            }

            switch (command.toLowerCase()){
                case "exit":
                    return;
                case "add" :
                    add(email);
                    System.out.println("Введите команду:");
                    break;
                case "list" :
                    list(email);
                    System.out.println("Введите команду:");
                    break;
                default:
                    System.out.println("Вы ввели неверную команду, повторите ввод:");
                    break;
            }
        }

    }

    static void add(String str){
        String[] test = str.split("@");
        if (str.split("@").length == 2 && str.split("\\.").length == 2 && !emailSet.contains(str)){
            emailSet.add(str);
        } else if (emailSet.contains(str)){
            System.out.println(str + " уже есть в списке");
        } else {
            System.out.println("вы ввели некорректный email, повторите");
        }
    }

    static  void list(String str1){
        if (!str1.isEmpty()){
            System.out.println("Вы ввели неверную команду");
        } else if (emailSet.isEmpty()){
            System.out.println("Список пуст");
        } else {
            for (String str : emailSet) {
                System.out.println(str);
            }
        }
    }
}
