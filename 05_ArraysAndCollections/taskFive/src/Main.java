import java.lang.reflect.Array;
import java.util.*;

public class Main {

    private static ArrayList<String> allCarNumber = new ArrayList<>();
    private static String enterNumber;
    private static HashSet<String> setCarNumbers = new HashSet<>();
    private static TreeSet<String> treeSetCarNumbers = new TreeSet<>();
    private static String result;

    public static void main(String[] args) {
        Character[] letter = new Character[] {'C' , 'M' , 'T' , 'B' , 'A', 'P' , 'O' , 'H' , 'E', 'Y'};
        Scanner sc = new Scanner(System.in);
        String text;

//        StringBuilder carNumber= new StringBuilder(); //xNNNyzR
//        StringBuilder carNumber;
        String stringNumber;
        int count = 0;
        for( int j = 1 ; j < 198 ; j ++) {
            for (int i = 100; i < 1000; i++) {
                stringNumber = String.valueOf(i);
                if (stringNumber.charAt(0) == (stringNumber.charAt(1)) && stringNumber.charAt(2) == (stringNumber.charAt(1))) {
                    for (Character character : letter) {
                        for (Character character1 : letter) {
                            for (Character character2 : letter) {
                                /*разница между билдером и конкатенацией строкине очевидна, судя по замеру.
                                но билдер чаще быстрее работает, но не всегда.
                                Возможно из-за того, что билдер необходимо создавать здесь в цикле.
                                 */
                                StringBuilder carNumber = new StringBuilder(); //xNNNyzR
                                carNumber.append(character);
                                carNumber.append(i).append(character1).append(character2);
                                carNumber.append(j);
                                allCarNumber.add(carNumber.toString());
                            }
                        }
                    }
                }
            }
        }
        setCarNumbers.addAll(allCarNumber);
        treeSetCarNumbers.addAll(allCarNumber);
        System.out.println("введите искомый номер");

        while (sc.hasNextLine()){
            text = sc.nextLine();
            enterNumber = text.toUpperCase();
            if (enterNumber.equalsIgnoreCase("exit")){
                break;
            }
            searchDirect();
            searchBinary();
            searchHashSet();
            searchTreeSet();

            System.out.println();
            System.out.println("Для продолжения введите номер или exit");
        }
    }
    private static void searchDirect(){
        long startTime = System.nanoTime();
        allCarNumber.contains(enterNumber);
        long duration = System.nanoTime() - startTime;
        if (allCarNumber.contains(enterNumber)){
            result = "номер найден";
        } else {
            result = "номер не найден";
        }
        System.out.println("Время прямого поиска: " + duration + ", " + result);
    }

    private static  void searchBinary(){
        String result;
        Collections.sort(allCarNumber);
        long startTime = System.nanoTime();
        Collections.binarySearch(allCarNumber, enterNumber);
        long duration = System.nanoTime() - startTime;
        if (Collections.binarySearch(allCarNumber, enterNumber) >=0 ){
            result = "номер найден";
        } else {
            result = "номер не найден";
        }
        System.out.println("Время бинарного поиска: " + duration + ", " + result);
    }

    private static  void searchHashSet(){
        long startTime = System.nanoTime();
        setCarNumbers.contains(enterNumber);
        long duration = System.nanoTime() - startTime;
        if (setCarNumbers.contains(enterNumber)){
            result = "номер найден";
        } else {
            result = "номер не найден";
        }
        System.out.println("Время HashSEt поиска: " + duration + ", " + result);
    }

    private static void searchTreeSet(){
        long startTime = System.nanoTime();
        treeSetCarNumbers.contains(enterNumber);
        long duration = System.nanoTime() - startTime;
        if (treeSetCarNumbers.contains(enterNumber)){
            result = "номер найден";
        } else {
            result = "номер не найден";
        }
        System.out.println("Время TreeSet поиска: " + duration + ", " + result);
    }
}
