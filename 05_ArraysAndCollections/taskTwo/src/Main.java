
import java.util.ArrayList;
import java.util.Scanner;

public class Main {

    static ArrayList<String> myList = new ArrayList<>();

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String command;
        int number = 0;

        System.out.println("Введите команду. Для помощи введите help. Для выхода введите exit");
        while (sc.hasNextLine()) {
            command = sc.nextLine();
            String[] subCommand = command.split(" ", 2);

            switch (subCommand[0].toLowerCase()) {
                case "exit":
                    return;
                case "help":
                    System.out.println("list - выведет список дел; \n" +
                            "add text- добавит дело 'text' в список; \n" +
                            "add number text - добавит дело 'text' в список под номером 'number'. Например, add 4 text; \n" +
                            "edit number text- заменит дело с номером 'number' на дело 'text'; \n" +
                            "delete number - удалит дело под порядковым номером 'number';" +
                            "exit - выход из программы");
                    System.out.println("Введите команду:");
                    break;
                case "list":
                    listCommand(command);
                    System.out.println("Введите команду:");
                    break;
                case "add":
                    addCommand(command);
                    System.out.println("Введите команду:");
                    break;
                case "edit":
                    editCommand(command);
                    System.out.println("Введите команду:");
                    break;
                case "delete":
                    deleteCommand(command);
                    System.out.println("Введите команду:");
                    break;
                default:
                    System.out.println("Не верно введена команда, повторите ввод");
                    System.out.println("Введите команду:");
                    break;
            }

        }
    }

    static void addCommand( String str){
        String[] subCommand =  str.split(" ", 2);
        int number;
        if (subCommand[1].split(" ", 2).length > 1) {
            number = Integer.parseInt(subCommand[1].split(" ", 2)[0]);
            if (number <= myList.size()) {
                myList.add(number, subCommand[1].split(" ", 2)[1]);
            } else {
                myList.add(subCommand[1].split(" ", 2)[1]);
            }
        } else {
            myList.add(subCommand[1]);
        }
    }

    static void listCommand(String str){
        if (myList.isEmpty()){
            System.out.println("Список пуст.");
        }
        if (str.split(" ").length > 1){
            System.out.println("Не верно введена команда, повторите ввод");
        } else {
                for (int i = 0; i < myList.size(); i++) {
                    System.out.println(i + " - " + myList.get(i));
                }
        }
    }

    static void editCommand(String str){
        String[] subCommand =  str.split(" ", 2);
        int number;
            if (str.split(" ", 2).length > 1 && subCommand[1].split(" ", 2).length>1) {
                number = Integer.parseInt(subCommand[1].split(" ", 2)[0]);
                if (number > myList.size()){
                    System.out.println("Вы пытаетесь изменить не существующую запись. В 'списке дел' записей: " + myList.size() +
                            ". Для подробной информации введите list");
                } else {
                    myList.remove(number);
                    myList.add(number, subCommand[1].split(" ", 2)[1]);
                }
            } else  {
                System.out.println("Не верно введена команда, повторите ввод");
            }
    }

    static void deleteCommand(String str){
        String[] subCommand = str.split( " ", 2);
        int number;
        if (str.split(" ", 2).length > 1) {
            number = Integer.parseInt(subCommand[1].split(" ", 2)[0]);
            if (number <= myList.size()) {
                System.out.println(number);
                myList.remove(number);
            } else {
                System.out.println("Вы пытаетесь удалить не существующую запись. В 'списке дел' записей: " + myList.size() +
                        ". Для подробной информации введите list");
            }
        } else if (myList.size() == 0 && str.split(" ", 2).length <= 1) {
            System.out.println("Список пуст");
        } else {
                System.out.println("Не верно введена команда, повторите ввод");
        }
    }

}
