
import java.io.PrintStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class Main {
    public Main() {
    }

    public static void main(String[] args) {
        //задание 1
        System.out.println("Задание с радугой");
        String rainbow = "красный оранжевый желтый зеленый голубой синий фиолетовый";
        String[] rainbowColors = rainbow.split("\\s+");

        for(String color : rainbowColors) {
            System.out.println(color);
        }

        System.out.println();
        System.out.println("Радуга в обратную сторону:");

        for(int itemRainbowColors = rainbowColors.length - 1; itemRainbowColors >= 0; --itemRainbowColors) {
            System.out.println(rainbowColors[itemRainbowColors]);
        }
        System.out.println();
        //задание 2
        System.out.println("Задание с температурой:");
        double averageAmount = 0.0d;
        double sumTemperature = 0.0d;
        int countHealthyPatient = 0;
        double[] patientTemperatures = new double[30];

        for(int i = 0; i < patientTemperatures.length; ++i) {
            patientTemperatures[i] = (double)Math.round((32 + Math.random() * 8) * 10) / 10;


            if (patientTemperatures[i] > 36 && patientTemperatures[i] <= 36) {
                ++countHealthyPatient;
            }
            sumTemperature += patientTemperatures[i];
            averageAmount = sumTemperature / (double)(i + 1);
            System.out.print(patientTemperatures[i] + " ");
        }

        System.out.println();
        double averageTemperature = (double)Math.round(averageAmount * 10.0D);
        System.out.println("Средняя температура по больнице: " + averageTemperature / 10.0D);
        System.out.println("Количество здоровых пациентов: " + countHealthyPatient + "\n");

        //задание 3
        System.out.println("крестик-массив:");
        Scanner sc = new Scanner(System.in);
        System.out.println("введите количество строк:");
        int row = sc.nextInt();
        String[][] array = new String[row][row];
        int i;
        int j;
        if (row % 2 != 0) {
            for(i = 0; i < array.length; ++i) {
                for(j = 0; j < array[i].length; ++j) {
                    if (i != j && j != row - 1 - i) {
                        array[i][j] = " ";
                    } else {
                        array[i][j] = "x";
                    }
                    System.out.print(array[i][j]);
                }
                System.out.println();
            }
        } else {
            for(i = 0; i < array.length; ++i) {
                for(j = 0; j < array[i].length; ++j) {
                    if ((i != j || i == array[i].length - 1) && j != row - 2 - i) {
                        array[i][j] = " ";
                    } else {
                        array[i][j] = "x";
                    }
                    System.out.print(array[i][j]);
                }
                System.out.println();
            }
        }
    }
}
