import java.text.SimpleDateFormat;
import java.util.Date;

public class Employee {
    private String typeOfAccount;
    private String accountNumber;
    private String accountCurrency;
    private Date dateOfOperation;
    private String referenceWiring;
    private String operationDescription;
    private Double incoming;
    private Double expenditure;

    public Employee(String typeOfAccount, String accountNumber, String accountCurrency, Date dateOfOperation, String referenceWiring, String operationDescription, Double incoming, Double expenditure) {
        this.typeOfAccount = typeOfAccount;
        this.accountNumber = accountNumber;
        this.accountCurrency = accountCurrency;
        this.dateOfOperation = dateOfOperation;
        this.referenceWiring = referenceWiring;
        this.operationDescription = operationDescription;
        this.incoming = incoming;
        this.expenditure = expenditure;
    }

    public Employee(String operationDescription, Double expenditure){
        this.operationDescription = operationDescription;
        this.expenditure = expenditure;
    }


    public String getTypeOfAccount() {
        return typeOfAccount;
    }

    public void setTypeOfAccount(String typeOfAccount) {
        this.typeOfAccount = typeOfAccount;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getAccountCurrency() {
        return accountCurrency;
    }

    public void setAccountCurrency(String accountCurrency) {
        this.accountCurrency = accountCurrency;
    }

    public Date getDateOfOperation() {
        return dateOfOperation;
    }

    public void setDateOfOperation(Date dateOfOperation) {
        this.dateOfOperation = dateOfOperation;
    }

    public String getReferenceWiring() {
        return referenceWiring;
    }

    public void setReferenceWiring(String referenceWiring) {
        this.referenceWiring = referenceWiring;
    }

    public String getOperationDescription() {
        return operationDescription;
    }

    public void setOperationDescription(String operationDescription) {
        this.operationDescription = operationDescription;
    }

    public Double getIncoming() {
        return incoming;
    }

    public void setIncoming(Double incoming) {
        this.incoming = incoming;
    }

    public Double getExpenditure() {
        return expenditure;
    }

    public void setExpenditure(Double expenditure) {
        this.expenditure = expenditure;
    }

}
