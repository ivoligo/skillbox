import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicReference;

public class Main {
//    private static final File CSV_FILE = new File("../files/movementList.csv");
    private static final File CSV_FILE = new File("data/movementList.csv");
    private static final String dateFormat = "dd.MM.yyyy";

    public static void main(String[] args) {
        infoOutput();
    }

    private static File conversionCsvFile(File file) throws IOException {
        StringBuilder builder = new StringBuilder();
        List<String> lines = Files.readAllLines(Paths.get(String.valueOf(file)));
        /*
        удаляю первую строчку, так как в ней содержаться названия столбцов.
        так же исхожу из того, что названия столцов присутствуют всегда, даже если выписка пустая.
         */
        if (lines.size() > 1) {
            lines.remove(0);
        }
        /*
        ниже исхожу из того,что приход тоже может иметь дробную часть(то есть содержать копейки)
        также для дальнейшего преобразования в double в числах с копейками(в приходе и расходе) заменяю запятую на точку.
         */
        for (String lineR : lines){
            String[] newLine = lineR.split(",\"");
            for (String string : newLine){
                string = string.replaceAll(",", ";");
                if (string.contains("\"")) {
                    string = string.replaceAll(";", ",");
                    string = string.replaceAll("\",", ";");
                    string = string.replaceAll("\"", "");
                    string = string.replaceAll(",", ".");
                    builder.append(";");
                }
                builder.append(string);
            }
            builder.append("\n");
        }
        return new File(String.valueOf(Files.write(Paths.get("data/modified.txt"), Collections.singleton(builder))));
    }

    private static ArrayList<Employee> parseCsvFile(){
        ArrayList<Employee> dataOperation = new ArrayList<>();

        try {
            File conversionCsvFile = conversionCsvFile(CSV_FILE);
            List<String> lines = Files.readAllLines(Paths.get(String.valueOf(conversionCsvFile)));
            for (String line : lines){
                String[] field = line.split(";");
                if (field.length != 8 && !(line.isEmpty())) {
                    System.out.println("Ошибка чтения строки: " + line);
                    continue;
                }
                if (!(line.isEmpty())) {
                    dataOperation.add(new Employee(
                            field[0],
                            field[1],
                            field[2],
                            (new SimpleDateFormat(dateFormat)).parse(field[3]),
                            field[4],
                            field[5],
                            Double.parseDouble(field[6]),
                            Double.parseDouble(field[7])
                    ));
                }
            }
        }catch ( Exception e) {
            e.printStackTrace();
        }
        return dataOperation;
    }

    // достать компании, чтобы посчитать по ним расходы
    private static Set<String> parseOperationDescriptionByExpenses(){
        String[] nameOfExpenses = null;
        ArrayList<Employee> dataOperation = parseCsvFile();
        ArrayList<String> listOfOperationDescription = new ArrayList<>();
        Set<String> listNameOfExpenses = new HashSet<>();
        dataOperation.forEach(employee -> {
            if (employee.getExpenditure() != 0) {
                listOfOperationDescription.add(employee.getOperationDescription());
            }
        });
        for(String string : listOfOperationDescription){
            String filedWithCompany = string.substring(16,70).trim();
            if (filedWithCompany.contains("\\")) {
                nameOfExpenses = filedWithCompany.split("\\\\");
            }
            if (filedWithCompany.contains("/")){
                nameOfExpenses = filedWithCompany.split("/");
            }
            if (nameOfExpenses != null) {
                for (String string1 : nameOfExpenses) {
                    if (string1.equals(nameOfExpenses[nameOfExpenses.length - 1])) {
                        listNameOfExpenses.add(string1.trim());
                    }
                }
            } else {
                System.out.println("Проверьте данные и при необходимости поправьте код на актуальное содержимое");
            }
        }
        return listNameOfExpenses;

    }

    private static void infoOutput(){
        ArrayList<Employee> myDataOperation = parseCsvFile();
        //приход
        double sumIncoming = myDataOperation.stream().mapToDouble(Employee::getIncoming).sum();
        System.out.println("Сумма доходов: "+ sumIncoming + " руб.");
        //расход
        double sumExpenditure = myDataOperation.stream().mapToDouble(Employee::getExpenditure).sum();
        System.out.println("Сумма расходов: " + sumExpenditure + " руб.");
        //расход по организациям
        Set<String> listNameOfExpenses = parseOperationDescriptionByExpenses();
        HashMap<String, Double> resultWhereAndHowMuch = new HashMap<>();
        for (String string : listNameOfExpenses) {
            double sumOfExpensesByName = 0.0;

            sumOfExpensesByName += myDataOperation.stream().filter(employee -> employee.getOperationDescription().contains(string)).mapToDouble(Employee::getExpenditure).sum();
            resultWhereAndHowMuch.put(string, sumOfExpensesByName);
        }
        System.out.println("Суммы расходов по организациям:");
        for (Map.Entry<String, Double> item : resultWhereAndHowMuch.entrySet()) {
            System.out.println(item.getKey() + " -  " + item.getValue() + " руб.");

        }
    }
}
