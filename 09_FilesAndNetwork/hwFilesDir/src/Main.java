import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите путь до папки:");
        String enterFolder;
        long size = 0;
        double sizeKb = 0.0d;
        double sizeMb = 0.0d;
        enterFolder = sc.nextLine();
        try {
            File searchFolder = new File(enterFolder);
            size = getSizeFolder(searchFolder);
            sizeKb = (double) Math.round((size / 1024d) * 100) / 100;
            sizeMb = (double) Math.round((sizeKb / 1024d) * 100) / 100;
            System.out.println("Размер \"" + enterFolder + "\" в байтах = " + size + " байт\n" +
                    "размер \"" + enterFolder + "\" в Кб = " + sizeKb + " Кб\n" +
                    "размер \"" + enterFolder + "\" в Mб = " + sizeMb + " Mb");
        } catch (Exception e){
        e.printStackTrace();
    }

    }

    public static long getSizeFolder(File folder){
        long size = 0;

        if (folder.isFile()) {
            size = folder.length();
        } else {
            File[] listFiles = folder.listFiles();
            for (File file : listFiles) {
                if (file.isFile()) {
                    size += file.length();
                } else {
                    size += getSizeFolder(file);
                }
            }
        }

        return size;
    }
}
