import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String source;
        String destination;
        System.out.println("Введите что скопировать(source):");
        source = sc.nextLine();
        System.out.println("Введите куда скопировать(destination):");
        destination = sc.nextLine();
        File sourceFile = new File(source);
        File destinationFile = new File(destination);
        copyDirectory(sourceFile,destinationFile);

    }

    public static void copyDirectory(File sourceFolder,File destinationFolder){
        if (destinationFolder == null) {
            destinationFolder.mkdir();
        }
        try {
            if (sourceFolder.exists()) {
                if (sourceFolder.isFile()) {
                    StringBuilder builder = new StringBuilder();
                    List<String> lines = Files.readAllLines(Paths.get(String.valueOf(sourceFolder)));
                    lines.forEach(line -> builder.append(line + "\n"));
                    Files.write(Paths.get(String.valueOf(destinationFolder)), lines);
                } else {
                    File[] listFiles = sourceFolder.listFiles();
                    for (File file : listFiles) {
                        if (file.isFile()) {
                            StringBuilder builder = new StringBuilder();
                            List<String> lines = Files.readAllLines(Paths.get(String.valueOf(file)));
                            lines.forEach(line -> builder.append(line + "\n"));
                            File copy = new File(destinationFolder + "\\" + file.getName());
                            Files.write(Paths.get(String.valueOf(copy)), lines);
                        } else {
                            File directory = new File(destinationFolder + "\\" + file.getName());
                            directory.mkdir();
                            copyDirectory(file, directory);
                        }
                    }
                }
            } else {
                System.out.println("Папка или файл указанный для копирования (source) отсутствует");
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
