import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

public class Main {
    public static void main(String[] args) throws IOException {
        File destinationFolder = new File("images");
        Document doc = Jsoup.connect("https://lenta.ru/").get();
        Elements linksImg = doc.select("img");
        linksImg.forEach(link -> {
            try {
//                if (!link.attr("alt").isEmpty()) {
                if (link.attr("abs:src").contains("lenta.ru")){
//                    System.out.println(link.attr("abs:src"));
//                    System.out.println(link.attr("alt"));
//                    PrintWriter writer = new PrintWriter("images/" + link.attr("alt"));
                    if (!destinationFolder.exists()){
                        destinationFolder.mkdir();
                    }
                    File file = new File(destinationFolder + "/" + link.attr("abs:src").substring(61));
                    FileOutputStream writer = new FileOutputStream(file);
                    Connection.Response image = Jsoup.connect(link.attr("abs:src")).ignoreContentType(true).execute();
                    writer.write(image.bodyAsBytes());
                    writer.flush();
                    writer.close();
                    System.out.println(link.attr("abs:src").substring(61));
                }
            } catch (IOException ex){
                ex.printStackTrace();
            }
        });
    }
}
