package model;

import java.util.List;

public class Connection {
    private Station stationFrom;
    private Station stationTo;

    public Connection(){

    }

    public Connection(Station stationFrom, Station stationTo) {
        this.stationFrom = stationFrom;
        this.stationTo = stationTo;
    }


    public Station getStationFrom() {
        return stationFrom;
    }

    public void setStationFrom(Station stationFrom) {
        this.stationFrom = stationFrom;
    }

    public Station getStationTo() {
        return stationTo;
    }

    public void setStationTo(Station stationTo) {
        this.stationTo = stationTo;
    }
}
