
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;


import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;

public class ParserJsonFile {


    public static JSONObject parseJsonFile(File file) throws ParseException {
        StringBuilder builder = new StringBuilder();
        try {
            List<String> lines = Files.readAllLines(Paths.get(String.valueOf(file)));
            lines.forEach(builder::append);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        JSONParser parser = new JSONParser();
        JSONObject jsonData = (JSONObject) parser.parse(String.valueOf(builder));

        return jsonData;

    }
}
