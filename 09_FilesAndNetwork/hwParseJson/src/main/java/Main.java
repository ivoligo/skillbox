import model.Connection;
import model.Station;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.ParseException;

import java.io.*;
import java.util.*;

public class Main {

    static File file = new File("data/moscowMap.json");

    public static void main(String[] args) throws IOException, ParseException {

        CreatorJsonFile.createJsonFile(file);
        printNumberOfStation(file);
        System.out.println();
        printConn(file);

    }

    public static void printNumberOfStation(File file) throws ParseException {
        JSONObject jsonData = ParserJsonFile.parseJsonFile(file);

        JSONObject stations = (JSONObject) jsonData.get("stations");

        Map<String, List<String>> lineStations = new HashMap<>();
        lineStations.putAll(stations);
        for(Map.Entry<String, List<String>> entry : lineStations.entrySet()){
            String lineName = entry.getKey();

            int countStation = entry.getValue().size();
            System.out.println("Количество станций на линии \""+ lineName + "\": " + countStation);
        }
    }

    public static void printConn(File file) throws ParseException {
        JSONObject jsonData = ParserJsonFile.parseJsonFile(file);
        JSONArray connect = (JSONArray) jsonData.get("connections");
        Map<Station, List<Station>> connections = new LinkedHashMap<>();
        //всего переходов
        int countConnect = 0;
        /*
        countConnectRepeating -  количество станций, где повторяются переходы;
        (для количество переходов, нужно будет разделить на два)
         */
        int countConnectRepeating = 0;

        connect.forEach(test -> {
            JSONArray item = (JSONArray) test;
            JSONObject itemStationFromObject = (JSONObject) item.get(0);
            Station stationFrom = new Station(
                    (String) itemStationFromObject.get("lineNumberFrom"),
                    (String)    itemStationFromObject.get("stationNameFrom")
            );

            JSONArray itemTo = (JSONArray) item.get(1);
            List<Station> listStationTo = new ArrayList<>();

            itemTo.forEach(stationToObject -> {
                JSONObject itemStationToObject = (JSONObject) stationToObject;
                Station stationTo = new Station(
                        (String) itemStationToObject.get("lineNumberTo"),
                        (String) itemStationToObject.get("stationNameTo")
                );
                listStationTo.add(stationTo);
            });
            connections.put(stationFrom , listStationTo);
        });

        Map<Station, Station> intermediateConnect = new LinkedHashMap<>();
        Map<Station, Station> connectsAll = new LinkedHashMap<>();

        for(Map.Entry<Station, List<Station>> entry : connections.entrySet()) {
            for(Station station : entry.getValue()){
                Connection connectForCompare = new Connection(entry.getKey(), station);
                intermediateConnect.put(station, entry.getKey());
                for(Map.Entry<Station, Station> entryForCompare : intermediateConnect.entrySet()) {
                    if((connectForCompare.getStationFrom().getName().equals( entryForCompare.getKey().getName())
                            && connectForCompare.getStationFrom().getNumberLine().equals(entryForCompare.getKey().getNumberLine()))
                            && ( connectForCompare.getStationTo().getName().equals(entryForCompare.getValue().getName())
                            && connectForCompare.getStationTo().getNumberLine().equals(entryForCompare.getValue() .getNumberLine()))){

                        connectsAll.put(station, entry.getKey());
//                                      System.out.println("со станции "+entryForCompare.getKey().getName() + " " + entryForCompare.getKey().getNumberLine());
//                                      System.out.println("на станцию " + entryForCompare.getValue().getName() + " " + entryForCompare.getValue().getNumberLine());
                        countConnect++;
                    }
                }
            }
        }

        System.out.println("Количество переходов в метро: " + countConnect );

    }
}
