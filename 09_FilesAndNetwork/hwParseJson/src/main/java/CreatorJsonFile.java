import model.Line;
import model.Station;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class CreatorJsonFile {
    static String linkForParse = "https://www.moscowmap.ru/metro.html#lines";

    private static List<Line> parseLinkForLine(String link) throws IOException {

        List<Line> listOfLine = new ArrayList<>();
        Document doc = Jsoup.connect(link).maxBodySize(0).get();
        Elements linesName = doc.select("div.s-depend-control-single");
        linesName.forEach(line ->{
            Line lineForList = new Line (line.select("span.js-metro-line").attr("data-line"), line.text());
            listOfLine.add(lineForList);
        });

        return listOfLine;

    }

    private static Map<String, List<String>> parseLinkForStation(String link) throws IOException {

        Map<String, List<String>> listOfStations = new LinkedHashMap<>();
        Document doc = Jsoup.connect(link).maxBodySize(0).get();
        Elements stations = doc.select("div.js-metro-stations");
        stations.forEach(station ->{
            List<String> listOfStationsName = new ArrayList<>();
            Elements stationName = station.select("span.name");
            for(Element string : stationName) {
                listOfStationsName.add(string.text());
            }
            listOfStations.put(station.attr("data-line"), listOfStationsName);
        });

        return listOfStations;
    }

    private static Map<Station, List<Station>> parseForConnections(String link) throws IOException {

        Map<Station, List<Station>> perehod = new LinkedHashMap<>();
        List<Line> test = parseLinkForLine(link);
        Document doc = Jsoup.connect(link).maxBodySize(0).get();
        Elements stations = doc.select("div.js-metro-stations");

        stations.forEach(station -> {
            Elements allLineTo = station.select("a:has(span.t-icon-metroln)");
            String dataline = station.attr("data-line");

            for (Element line : allLineTo) {
                List<Station> listOfStationsTo = new ArrayList<>();
                String stationFromString = line.select("span.name").text();
//                System.out.println("откуда " + stationFromString + " " + dataline);
                Station stationFrom = new Station(dataline, stationFromString);
                Elements linesTo = line.select("span[class*=t-icon-metroln ln]");

                for (Element element : linesTo) {
                    String lineTo = element.attr("class").substring(18);
                    String stationTo = element.attr("title").substring(20);
                    String[] testStation = stationTo.split("»");
                    stationTo = testStation[0];
//                    System.out.println("пересадка на "+stationTo + " " + lineTo);

                    for (Line lineForConn : test) {

                        if (lineForConn.getNumber().equals(lineTo)) {
                            Station stationForAdd = new Station(lineForConn.getNumber(), stationTo);
//                            System.out.println("проверка:");
//                            System.out.println(stationForAdd.getNumberLine() + " " + stationForAdd.getName());
                            listOfStationsTo.add(stationForAdd);
                        }
                    }
                }
//                System.out.println("проверка перед put. совпадать с откуда: " + stationFrom.getName() + " " + stationFrom.getNumberLine());
                perehod.put(stationFrom, listOfStationsTo);
            }
        });

        return perehod;
    }


    @SuppressWarnings("unchecked")
    public static void createJsonFile(File file) throws IOException {

        List<Line> listOfLine;
        Map<String, List<String>> listOfStation;
        List<JSONObject> listLine = new ArrayList<>();
        File destinationFolder = new File("data");

        if (!destinationFolder.exists()){
            destinationFolder.mkdir();
        }

        JSONObject outJsonForFile = new JSONObject();
        listOfLine =  parseLinkForLine(linkForParse);
        listOfStation = parseLinkForStation(linkForParse);
        FileWriter writer = new FileWriter(file);

        //получаю линии метро(номер+название)
        for (Line line : listOfLine){
            JSONObject forLine = new JSONObject();
            forLine.put("number", line.getNumber());
            forLine.put("name", line.getName());
            listLine.add(forLine);
        }
        outJsonForFile.put("lines",listLine);

        //список станций по линиям
        Map<String, List<String>> orderedStation = new LinkedHashMap<>();
        for(Map.Entry<String, List<String>> entry : listOfStation.entrySet()){
            orderedStation.put(entry.getKey(), entry.getValue());
        }
        outJsonForFile.put("stations",orderedStation);

        //переходы
        Map<Station, List<Station>> connections;
        List<JSONArray> listConnect = new ArrayList<>();
        connections = parseForConnections(linkForParse);
        for(Map.Entry<Station, List<Station>> entry : connections.entrySet()){
            JSONObject stationFrom = new JSONObject();
            List<JSONObject> listStationTo = new ArrayList<>();
            JSONArray stationFromTo = new JSONArray();
            stationFrom.put("stationNameFrom", entry.getKey().getName());
            stationFrom.put("lineNumberFrom", entry.getKey().getNumberLine());
            stationFromTo.add(stationFrom);
            for(Station station : entry.getValue()) {
                JSONObject stationTo = new JSONObject();
                stationTo.put("stationNameTo" , station.getName());
                stationTo.put("lineNumberTo", station.getNumberLine());
                listStationTo.add(stationTo);

            }
            stationFromTo.add(listStationTo);
            listConnect.add(stationFromTo);
        }

        outJsonForFile.put("connections", listConnect);

        writer.write(outJsonForFile.toJSONString());
        writer.flush();
    }

}
