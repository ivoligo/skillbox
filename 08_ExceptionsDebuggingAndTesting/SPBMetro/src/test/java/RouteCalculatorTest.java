import core.Line;
import core.Station;
import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.List;

public class RouteCalculatorTest extends TestCase {

    List<Station> route;
    List<Station> route1;

    @Override
    protected void setUp() throws Exception {
        route = new ArrayList<>();
        route1 = new ArrayList<>();

        Line line1 = new Line(1, "first");
        Line line2 = new Line(2, "second");
        Line line3 = new Line(3, "third");
        Line line4 = new Line(4, "fourth");
        Line line5 = new Line(5, "own");
        // Станции на line1
        Station station1 = new  Station("Sun", line1);
        Station station2 = new Station("Mercury", line1);
        Station station3 = new Station("Venus", line1);
        // Станции на line2
        Station station4 = new Station("Earth", line2);
        Station station5 = new Station("Moon", line2);
        // Станции на line3
        Station station6 = new Station("Mars", line3);
        Station station7 = new Station("Jupiter", line3);
        Station station8 = new Station("Saturn", line3);
        // Станции на line4
        Station station9 = new Station("Uranus", line4);
        Station station10 = new Station("Neptune", line4);
        Station station11 = new Station("Pluto", line4);
        // Станции на line5. отдельная ветка, ни с какой не пересекается
        Station station12 = new Station("Alpha", line5);
        Station station13 = new Station("Omega", line5);

        //
        route1.add(station1);
        route1.add(station2);
        route1.add(station3);
        route1.add(station4);
        route1.add(station5);
//        route1.add(station6);
//        route1.add(station7);
//        route1.add(station8);
//        route1.add(station9);
//        route1.add(station10);

        route.add(station1);
        route.add(station2);
        route.add(station3);
        route.add(station4);
        route.add(station5);
        route.add(station6);
        route.add(station7);
        route.add(station8);
        route.add(station9);
        route.add(station10);
        route.add(station11);
//        route.add(station12);
//        route.add(station13);

        StationIndex stationIndex = new StationIndex();

        //добавление станций, где пересекаются линии
        // пересечение line1+line2 это пересадка Sun-Earth
        List<Station> connectLine1Line2 = new ArrayList<>();
        connectLine1Line2.add(station1);
        connectLine1Line2.add(station4);
        stationIndex.addConnection(connectLine1Line2);
        // пересечение line1-line3 это пересадка Venus-Mars
        List<Station> connectLine1Line3 = new ArrayList<>();
        connectLine1Line3.add(station3);
        connectLine1Line3.add(station6);
        stationIndex.addConnection(connectLine1Line3);
        //пересечение line3-line4 это пересадка Saturn-Uranus
        List<Station> connectLine3Line4 = new ArrayList<>();
        connectLine3Line4.add(station8);
        connectLine3Line4.add(station9);
        stationIndex.addConnection(connectLine3Line4);

        //добавляю линии
        stationIndex.addLine(line1);
        stationIndex.addLine(line2);
        stationIndex.addLine(line3);
        stationIndex.addLine(line2);
        stationIndex.addLine(line4);

        //добавляю станции
        stationIndex.addStation(station1);
        stationIndex.addStation(station2);
        stationIndex.addStation(station3);
        stationIndex.addStation(station4);
        stationIndex.addStation(station5);
        stationIndex.addStation(station6);
        stationIndex.addStation(station7);
        stationIndex.addStation(station8);
        stationIndex.addStation(station9);
        stationIndex.addStation(station10);
        stationIndex.addStation(station11);

        //добавляю станции к линиям
        //line1
        line1.addStation(station1);
        line1.addStation(station2);
        line1.addStation(station3);
        //line2
        line2.addStation(station4);
        line2.addStation(station5);
        //line3
        line3.addStation(station6);
        line3.addStation(station7);
        line3.addStation(station8);
        //line4
        line4.addStation(station9);
        line4.addStation(station10);
        line4.addStation(station11);

        RouteCalculator routeCalculator = new RouteCalculator(stationIndex);
        //без пересадок
        routeCalculator.getShortestRoute(station1, station3);
        //одна пересадка
        routeCalculator.getShortestRoute(station1, station7);
        //две пересадки
        routeCalculator.getShortestRoute(station1, station10);
//        //не соединены
//        routeCalculator.getShortestRoute(station12, station5);

    }

    public void testCalculateDuration(){
        double actual = RouteCalculator.calculateDuration(route1);
        double expected = 11.0;
        assertEquals(expected, actual);
    }

}
