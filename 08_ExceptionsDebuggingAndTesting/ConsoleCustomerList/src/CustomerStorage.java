import java.util.HashMap;
import java.util.regex.Pattern;

public class CustomerStorage
{
    private HashMap<String, Customer> storage;

    public CustomerStorage()
    {
        storage = new HashMap<>();
    }

    public void addCustomer(String data) throws MyException {
        String[] components = data.split("\\s+");
        if (components.length !=4) {
            throw new MyException("Неверный формат команды add.\nКорректный формат: \n" +
                    "add Василий Петров vasily.petrov@gmail.com +79215637722");

        }
        if (components[3].length() != 12
                || !(components[3].matches("^\\+[0-9]*"))
        ){
            throw new MyException("Неверный формат телефонного номера.\nКорректный формат номера телефона: \n" +
                    "+79215637722");
        }
        if (!components[2].matches("\\w+\\.?\\w+@\\w+\\.[a-zA-Z]+")) {
            throw new MyException("Неверный формат email.\nКорректный формат почты: \n" +
                    "vasily.petrov@gmail.com или vasily@gmail.com или vasily_petrov@gmail.com");
        }
        String name = components[0] + " " + components[1];
        storage.put(name, new Customer(name, components[3], components[2]));
    }

    public void listCustomers()
    {
        storage.values().forEach(System.out::println);
    }

    public void removeCustomer(String name)
    {
        storage.remove(name);
    }

    public int getCount()
    {
        return storage.size();
    }
}