package App;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Random;

@RestController
public class FirstController {

//    @RequestMapping("/currentdate")
    @RequestMapping("/")
    public String currentDate(){
        long number = (long) (Math.random()* (Long.MAX_VALUE));
        String forReturn = "Сегодняшняя дата: \n" +
                (new Date()).toString()+"\n"+
                "Случайное число: " + number;
        return forReturn;
    }
}
