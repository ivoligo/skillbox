import account.CardAccount;
import account.DepositaryAccount;
import account.OwnAccount;

import java.util.Scanner;

public class Main {
    private static Scanner sc = new Scanner(System.in);
    private static String text;
    private static OwnAccount account = new OwnAccount();
    private static DepositaryAccount depositaryAccount = new DepositaryAccount();
    private static CardAccount cardAccount = new CardAccount();
    private static int money;

    public static void main(String[] args) {

        System.out.println("Выберете счет для операции:");
        System.out.println("1 - расчетный счет\n" +
                "2 - депозитный счет\n" +
                "3 - карточный счет\n" +
                "exit - для выхода\n");

        while (sc.hasNextLine()) {
            text = sc.nextLine();
            switch (text) {
                case "exit":
                    return;
                case "1":
                    ownAccountWork();
                    System.out.println("Выберете счет  или exit");
                    break;
                case "2":
                    depositaryAccountWork();
                    System.out.println("Выберете счет или exit");
                    break;
                case "3":
                    cardAccountWork();
                    System.out.println("Выберете счетили exit");
                    break;
                default:
                    System.out.println("ошибка ввода");
                    break;
            }
        }
    }

    private static void ownAccountWork() {
        System.out.println("расчетный счет. вы можете:");
        System.out.println("1 - положить деньги \n2 - снять деньги \n3 - проверить баланс \nexit - переход к выбору счета");
        while (sc.hasNextLine()) {
            text = sc.nextLine();
            switch (text) {
                case "1":
                    System.out.println("введите сумму:");
                    money = sc.nextInt();
                    account.inMoney(money);
                    break;
                case "2":
                    System.out.println("введите сумму:");
                    money = sc.nextInt();
                    account.outMoney(money);
                    break;
                case "3":
                    account.showMoney();
                    break;
                case "exit":
                    System.out.println("Вы перешли к выбору счета. выберете счет");
                    return;
                default:
                    System.out.println("введите номер операции:");
                    break;
            }
        }
    }

    private static void depositaryAccountWork() {
        System.out.println("Депозитный счет. вы можете:");
        System.out.println("1 - положить деньги \n2 - снять деньги \n3 - проверить баланс\nexit - переход к выбору счета");
        while (sc.hasNextLine()) {
            text = sc.nextLine();
            switch (text) {
                case "1":
                    System.out.println("введите сумму:");
                    money = sc.nextInt();
                    depositaryAccount.inMoney(money);
                    break;
                case "2":
                    System.out.println("введите сумму:");
                    money = sc.nextInt();
                    depositaryAccount.outMoney(money);
                    break;
                case "3":
                    depositaryAccount.showMoney();
                    break;
                case "exit":
                    System.out.println("Вы перешли к выбору счета. выберете счет");
                    return;
                default:
                    System.out.println("введите номер операции:");
                    break;
            }
        }
    }

    public static void cardAccountWork() {
        System.out.println("Карточный  счет. Вы можете ");
        System.out.println("1 - положить деньги \n2 - снять деньги \n3 - проверить баланс\nexit - переход к выбору счета");
        while (sc.hasNextLine()) {
            text = sc.nextLine();
            switch (text) {
                case "1":
                    System.out.println("введите сумму:");
                    money = sc.nextInt();
                    cardAccount.inMoney(money);
                    break;
                case "2":
                    System.out.println("введите сумму:");
                    money = sc.nextInt();
                    cardAccount.outMoney(money);
                    break;
                case "3":
                    cardAccount.showMoney();
                    break;
                case "exit":
                    System.out.println("Вы перешли к выбору счета. выберете счет");
                    return;
                default:
                    System.out.println("введите номер операции:");
                    break;
            }
        }
    }
}
