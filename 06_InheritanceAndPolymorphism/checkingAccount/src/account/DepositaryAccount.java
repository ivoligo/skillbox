package account;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

public class DepositaryAccount extends OwnAccount {
    private Calendar inCalendar = Calendar.getInstance();
    private Calendar outCalendar = Calendar.getInstance();
    private long count;

    private DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");

    public DepositaryAccount() {
        super();
    }

    public int inMoney(int summ) {
        inCalendar.getTime();
        return super.inMoney(summ);
    }

    public double outMoney(double summ) {
        outCalendar.getTime();
        if ((long) ChronoUnit.DAYS.between(inCalendar.getTime().toInstant(), outCalendar.getTime().toInstant()) >= 30) {
            super.outMoney(summ);
        } else {
            System.out.println("Деньги не могут быть сняты. Месяц еще не прошел");
        }
        return super.outMoney(summ);
    }

}
