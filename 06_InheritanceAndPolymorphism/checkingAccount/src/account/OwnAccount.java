package account;


public class OwnAccount {
    private double accountSumm;

    public OwnAccount() {

    }

    public double getAccountSumm() {
        return accountSumm;
    }

    public void setAccountSumm(int accountSumm) {
        this.accountSumm = accountSumm;
    }

    public int inMoney(int summ) {
        accountSumm += summ;
        System.out.println("Деньги зачислены");
        return (int) accountSumm;
    }

    public double outMoney(double summ) {
        if (accountSumm - summ < 0) {
            System.out.println("Не хватает средств");
        } else {
            accountSumm -= summ;
            System.out.println("Деньги списались");
        }
        return accountSumm;
    }

    public void showMoney() {
        System.out.println("Баланс счета равен: " + accountSumm);
    }

}
