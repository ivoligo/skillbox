package account;

public class CardAccount extends OwnAccount {
    public CardAccount() {
        super();
    }

    public int inMoney(int summ) {
        return super.inMoney(summ);
    }

    public double outMoney(double summ) {
        summ = summ / 0.99;
        return super.outMoney(summ);
    }

}
