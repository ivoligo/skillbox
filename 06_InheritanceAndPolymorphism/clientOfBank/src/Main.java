import Accounts.*;

public class Main {
    public static void main(String[] args) {

        // физические лица
        System.out.println("физические лица");
        Client client = new FizAccount(1000);
        client.inMoney(200);
        client.showMoney();
        client.outMoney(590);
        client.showMoney();
        client.outMoney(2000);
        System.out.println();

        // индивидуальные предприниматели
        System.out.println("индивидуальные предприниматели");
        Client clientIP = new IpAccount(1000);
        clientIP.inMoney(200);
        clientIP.showMoney();
        clientIP.outMoney(590);
        clientIP.showMoney();
        clientIP.outMoney(2000);
        System.out.println();

        // юридические лица
        System.out.println("юридические лица");
        Client clientUr = new UrAccount(1000);
        clientUr.inMoney(200);
        clientUr.showMoney();
        clientUr.outMoney(590);
        clientUr.showMoney();
        clientUr.outMoney(2000);

        //тестовый счет
        Client clientTest = new TestAccount();
        clientTest.setAccountSumm(1000);
        clientTest.inMoney(200);
        clientTest.showMoney();
        clientTest.outMoney(590);
        clientTest.showMoney();
        clientTest.outMoney(2000);
    }
}
