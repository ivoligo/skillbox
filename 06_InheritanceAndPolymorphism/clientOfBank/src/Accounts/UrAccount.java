package Accounts;

public class UrAccount extends Client{

    public UrAccount(double accountSumm){
        super(accountSumm);
    }

    @Override
    public double inMoney(double sum) {
        setAccountSumm(getAccountSumm()+ sum);
        return getAccountSumm();
    }

    @Override
    public double outMoney(double sum) {
        if ((getAccountSumm() - sum) < 0){
            System.out.println("Недостаточно средств");
        } else {
            setAccountSumm(getAccountSumm() - (sum / 0.99));
        }
        return getAccountSumm();
    }

    @Override
    public void showMoney() {
        System.out.printf("Сумма на счете: %.2f %n", getAccountSumm());
    }

}
