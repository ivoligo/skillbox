package Accounts;

public abstract class Client {
    private double accountSumm;

    public Client(double accountSumm){
        this.accountSumm = accountSumm;
    }

    public Client() {

    }

    public double getAccountSumm() {
        return accountSumm;
    }

    public void setAccountSumm(double accountSumm) {
        this.accountSumm = accountSumm;
    }

    public abstract double inMoney(double sum);

    public abstract double outMoney(double sum);

    public abstract void showMoney();
}
