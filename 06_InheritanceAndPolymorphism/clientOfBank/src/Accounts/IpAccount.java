package Accounts;

public class IpAccount extends Client{

    public IpAccount(double accountSum){
        super(accountSum);
    }

    public double inMoney(double sum) {
        if (sum < 1000.0) {
            setAccountSumm(getAccountSumm() + sum * 0.99);
        } else {
            setAccountSumm(getAccountSumm() + sum * 0.995);
        }
        return getAccountSumm();
    }

    public double outMoney(double sum) {
        if ((getAccountSumm() - sum) < 0){
            System.out.println("Недостаточно средств");
        } else {
            setAccountSumm(getAccountSumm() - sum);
        }
        return getAccountSumm();
    }

    public void showMoney() {
        System.out.printf("Сумма на счете: %.2f %n", getAccountSumm());
    }
}
