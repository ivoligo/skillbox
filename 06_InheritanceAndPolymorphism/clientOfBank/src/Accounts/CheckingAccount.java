package Accounts;

public class CheckingAccount extends Client{

    public CheckingAccount(double accountSumm) {
        super(accountSumm);
    }

    public double inMoney(double sum) {
        setAccountSumm(getAccountSumm() + sum);
        return getAccountSumm();
    }

    public double outMoney(double sum) {
        if (getAccountSumm() > sum) {
            setAccountSumm(getAccountSumm() - sum);
        } else {
            System.out.println("Недостачно средст");
        }
        return getAccountSumm();
    }

    public void showMoney() {
        System.out.printf("Сумма на счете: %.2f %n", getAccountSumm());
    }

}
