package employees;

import companies.Company;

public class Operator implements Employee{
    private int fixOperatorSalary;
    Company company;

    public Operator(int fixOperatorSalary){
        this.fixOperatorSalary = fixOperatorSalary;
    }

    @Override
    public double getMonthSalary() {
        return fixOperatorSalary;
    }

    @Override
    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public int compareTo(Employee o) {
        return Double.compare( o.getMonthSalary(),  getMonthSalary());
    }
}
