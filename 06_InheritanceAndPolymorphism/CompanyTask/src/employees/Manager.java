package employees;

import companies.Company;

public class Manager implements Employee {

    private int fixManagerSalary;
    Company company;

    public Manager (int fixManagerSalary){
        this.fixManagerSalary = fixManagerSalary;
    }

    @Override
    public double getMonthSalary() {
        double managerIncome =  115000 + Math.random()*25000 ;
        return (double) Math.round((fixManagerSalary + managerIncome * 0.05)* 100) / 100 ;
    }

    @Override
    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public int compareTo(Employee o) {
        return Double.compare( o.getMonthSalary(), getMonthSalary());
    }
}
