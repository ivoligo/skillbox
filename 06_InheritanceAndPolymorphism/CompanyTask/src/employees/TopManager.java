package employees;

import companies.Company;

public class TopManager implements Employee{
    private int fixTopManagerSalary;
    Company company;

    public TopManager (int fixTopManagerSalary){
        this.fixTopManagerSalary = fixTopManagerSalary;
    }

    @Override
    public double getMonthSalary() {
        setCompany(company);
        if (company.getIncome() > 10000000) {
            return (double) Math.round ((fixTopManagerSalary + 1.5 * fixTopManagerSalary) * 100) / 100;
        } else {
            return fixTopManagerSalary;
        }
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }

    @Override
    public int compareTo(Employee o) {
        return Double.compare(o.getMonthSalary(), getMonthSalary());
    }
}
