package employees;

import companies.Company;

public interface Employee extends Comparable<Employee> {

    double getMonthSalary();
    void setCompany(Company company);

}
