import companies.Company;
import employees.Employee;
import employees.Manager;
import employees.Operator;
import employees.TopManager;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        //проверка с наймом по одному
        System.out.println("найм по одному:");
        Company test = new Company(11000000.00);
        Operator operator = new Operator(100000);

        for (int i = 0 ; i < 180 ;i ++){
            test.hire(new Operator(100000));
            if (i < 10){
                TopManager topManager = new TopManager(200000);
                topManager.setCompany(test);
                test.hire(topManager);
            }
            if (i < 80) {
                test.hire(new Manager(150000));
            }
        }
        System.out.println("сначала высокие");
        for (Employee e : test.getTopSalaryStaff(15)){
            System.out.println(e.getMonthSalary());
        }
        //увольнение
        for (int i = 0 ; i < 140 ; i++){
            test.fire(operator);
        }
        System.out.println("по возрастанию");

        for (Employee e : test.getLowestSalaryStaff(230)){
            System.out.println(e.getMonthSalary());
        }
        System.out.println();
        System.out.println();
        //проверка с наймом списка
        System.out.println("найм списком:");
        Company testAll = new Company(10000001);
        List<Employee> listOperator = new ArrayList<>();
        List<Employee> listManager = new ArrayList<>();
        List<Employee> listTopManager = new ArrayList<>();
        for (int i = 0 ; i < 180 ; i++){
            listOperator.add(new Operator(100000));
        }
        for (int i = 0 ; i < 10 ; i ++){
            TopManager topManager = new TopManager(100000);
            topManager.setCompany(testAll);
            listTopManager.add(topManager);
        }
        for (int i = 0 ; i < 80 ; i++){
            listManager.add(new Manager(100000));
        }
        testAll.hireAll(listOperator);
        testAll.hireAll(listManager);
        testAll.hireAll(listTopManager);

        for (Employee e : testAll.getTopSalaryStaff(30)){
            System.out.println(e.getMonthSalary());
        }
    }
}
