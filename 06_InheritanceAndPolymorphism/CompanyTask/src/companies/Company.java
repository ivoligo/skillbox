package companies;

import employees.Employee;
import employees.Manager;

import java.util.*;

public class Company {

    private double income;
    private List<Employee> employeeListWorkNow;

    public Company(double income){
        this.income = income;
        this.employeeListWorkNow = new ArrayList<>();
    }

    public Company(){

    }

    public List<Employee> hire(Employee employee) {
        employee.setCompany(this);
        this.employeeListWorkNow.add(employee);
        return this.employeeListWorkNow;
    }

    public List<Employee> hireAll(List<Employee> employeeListHire){
        for (Employee employee : employeeListHire){
            employee.setCompany(this);
        }
        this.employeeListWorkNow.addAll(employeeListHire);
        return this.employeeListWorkNow;
    }

    public List<Employee> fire(Employee employee){
        this.employeeListWorkNow.remove(employee);
        return this.employeeListWorkNow;
    }

    public double getIncome() {
        return income;
    }

    public void setIncome(int income) {
        this.income = income;
    }

    public List<Employee>  getTopSalaryStaff(int count){
        ArrayList<Employee> list = new ArrayList<>();
        Collections.sort(employeeListWorkNow);
        if (count < employeeListWorkNow.size()) {
            for (int i = 0; i <= count; i++) {
                list.add(employeeListWorkNow.get(i));
            }
        }else {
            System.out.println("Сотрудников меньше чем вы ввели");
        }
        return list;
    }

    public ArrayList<Employee> getLowestSalaryStaff(int count){
        ArrayList<Employee> list = new ArrayList<>();
        Collections.sort(employeeListWorkNow);
        if (count < employeeListWorkNow.size()) {
            for (int i = (employeeListWorkNow.size()-1); i >= (employeeListWorkNow.size()-count); i--) {
                list.add(employeeListWorkNow.get(i));
            }
        }else {
            System.out.println("Сотрудников меньше чем вы ввели");
        }

        return list;
    }
}
